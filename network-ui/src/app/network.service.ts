import { Injectable } from '@angular/core';
import { Network } from './network';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  private networkUrl = 'http://localhost:8080/network';

  constructor(private http:HttpClient) { }

  getNetworks(): Observable<Network[]> {
    return this.http.get<Network[]>(this.networkUrl);
  }

  createNetwork(network:Network): Observable<Network> {
    return this.http.post<Network>(this.networkUrl, network, httpOptions)
      .pipe(tap( (net:Network) => console.log(`added network with id: ${net.id}`) ),
            catchError(this.handleError<Network>('createNetwork')));
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
