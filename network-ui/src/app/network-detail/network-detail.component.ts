import { Component, OnInit, Input } from '@angular/core';
import { Network } from '../network';
import { Node, Link } from '../d3';

@Component({
  selector: 'app-network-detail',
  templateUrl: './network-detail.component.html',
  styleUrls: ['./network-detail.component.scss']
})
export class NetworkDetailComponent implements OnInit {

  @Input() network: Network;
  @Input() watch:string;

  nodes: Node[] = [];
  links: Link[] = [];

  constructor() {
  }

  ngOnInit() {
  }

}
