import { Injectable } from '@angular/core';
import { Node } from './models/node';
import { Link } from './models/link';
import { ForceDirectedGraph } from './models/force-directed-graph';
import * as d3 from 'd3';

@Injectable()
export class D3Service {
    constructor() { }

  getForceDirectedGraph(nodes: Node[], links: Link[], options: { width, height }):ForceDirectedGraph {
    return new ForceDirectedGraph(nodes, links, options);
  }
}
