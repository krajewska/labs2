export class Node implements d3.SimulationNodeDatum {

  index?: number;
  x?: number;
  y?: number;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;

  id: string;
  nodeType:string;

  constructor(id, nodeType: string) {
    this.id = id;
    this.nodeType = nodeType;
  }
}
