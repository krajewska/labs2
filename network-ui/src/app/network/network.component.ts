import { Component, OnInit } from '@angular/core';
import { NetworkService } from '../network.service';
import { Network } from '../network';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {

  networks: Network[];
  selectedNetwork: Network;
  watch:string;

  constructor(private networkService: NetworkService) { }

  getNetworks(): void {
    this.networkService.getNetworks()
      .subscribe(networks => this.networks = networks);
  }

  ngOnInit() {
    this.getNetworks();
  }

  onSelect(network: Network): void {
    this.selectedNetwork = network;
    this.watch = network.name;
  }

}
