import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NetworkComponent } from './network/network.component';
import { NetworkDetailComponent } from './network-detail/network-detail.component';
import { NetworkCreateComponent } from './network-create/network-create.component';
import { GraphComponent } from './visuals/graph/graph.component';
import { LinkVisualComponent } from './visuals/shared/link-visual/link-visual.component';
import { NodeVisualComponent } from './visuals/shared/node-visual/node-visual.component';
import { D3Service } from './d3';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NetworkComponent,
    NetworkDetailComponent,
    NetworkCreateComponent,
    GraphComponent,
    LinkVisualComponent,
    NodeVisualComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [D3Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
