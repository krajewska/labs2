import { NetworkNode } from './network.node';
import { Node } from './d3/models/node';
import { Link } from './d3/models/link';


export class Network {
  id: number;
  name: string;
  description: string;

  consensusType: string;
  networkType: string;

  nodes:NetworkNode[];
  links: Link[];
}
