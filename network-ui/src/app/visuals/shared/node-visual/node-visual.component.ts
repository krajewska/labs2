import { Component, Input } from '@angular/core';
import { Node } from '../../../d3';

@Component({
  selector: '[nodeVisual]',
  template: `
    <svg:g [attr.transform]="'translate(' + node.x + ',' + node.y + ')'">
      <svg:circle
          cx="0"
          cy="0"
          r="30"
          [attr.fill]="getColor()">
      </svg:circle>
      <svg:text text-anchor="middle" stroke-width="2px" alignment-baseline="middle">
        {{node.id}}
      </svg:text>
    </svg:g>
  `
})
export class NodeVisualComponent {
  @Input('nodeVisual') node: Node;

  getColor():string {
    console.log(`node:${this.node.id} type:${this.node.nodeType}`);
    if (this.node.nodeType == 'audit') {
      return 'orange';
    } else {
      return 'lightblue';
    }
  }
}
