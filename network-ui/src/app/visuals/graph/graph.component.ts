import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { D3Service, ForceDirectedGraph, Node } from '../../d3';


@Component({
  selector: 'graph',
  template: `
    <svg #svg [attr.width]="_options.width" [attr.height]="_options.height">
      <g>
        <g [linkVisual]="link" *ngFor="let link of links"></g>
        <g [nodeVisual]="node" *ngFor="let node of nodes"></g>
      </g>
    </svg>
  `,
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnChanges {
  @Input('nodes') nodes;
  @Input('links') links;
  @Input('watch') watch;

  graph:ForceDirectedGraph;

  constructor(private d3Service: D3Service) { }

  ngOnInit() {
    this.graph = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);
  }

  ngAfterViewInit() {
    this.graph.initSimulation(this.options);
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let change = changes[propName];
      if (propName == 'watch') {
        this.graph = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);
        this.graph.initSimulation(this.options);
      }
    }
  }

  private _options: { width, height } = { width: 200, height: 200 };

  get options() {
    return this._options = {
      width: 400,
      height: 400
    };
  }
}
