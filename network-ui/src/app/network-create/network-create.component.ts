import { Component, OnInit } from '@angular/core';
import { Network } from '../network';
import { NetworkNode } from '../network.node';
import { NetworkService } from '../network.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-network-create',
  templateUrl: './network-create.component.html',
  styleUrls: ['./network-create.component.scss']
})
export class NetworkCreateComponent implements OnInit {

  model = new Network();
  submitted = false;

  constructor(private networkService:NetworkService, private router: Router) { }

  ngOnInit() {
  }

  save(): void {
    this.submitted = true;
    this.model.nodes = [];

    let auditNodeCount = parseInt(this.model.auditNodeCount);
    for (var i = 0 ; i < auditNodeCount; i++) {
      let node = new NetworkNode(`node${i+1}`, 'audit', `hashcode${i+1}`, `publickey${i+1}`);
      this.model.nodes.push(node);
    }

    let customerNodeCount = parseInt(this.model.customerNodeCount);
    for (var i = 0 ; i < customerNodeCount; i++) {
      let node = new NetworkNode(`node${i+1}`, 'customer', `hashcode${i+1}`, `publickey${i+1}`);
      this.model.nodes.push(node);
    }

    this.networkService.createNetwork(this.model)
      .subscribe(network => {
        this.router.navigate(['/network']);
      });
  }
}
