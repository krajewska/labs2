export class NetworkNode {
  id: number;
  name: string;
  nodeType: string;
  address: string;
  description: string;

  publickey: string;

  constructor(name: string, nodeType: string, address: string, publickey: string) {
    this.name = name;
    this.nodeType = nodeType;
    this.address = address;
    this.publickey = publickey;
  }
}
