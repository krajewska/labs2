
CREATE SCHEMA IF NOT EXISTS networkdb;

CREATE TABLE IF NOT EXISTS networkdb.network
(
    id integer NOT NULL,
    name text ,
    description text ,
    consensustype text ,
    networktype text ,
    CONSTRAINT network_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS networkdb.node
(
    id integer NOT NULL,
    networkid integer,
    address text,
    description text,
    name text,
    "publickey" text,
    nodetype text,
    CONSTRAINT node_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS networkdb.networkseq;
CREATE SEQUENCE IF NOT EXISTS networkdb.nodeseq;
