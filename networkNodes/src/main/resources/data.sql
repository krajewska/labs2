INSERT INTO networkdb.network(id, name, description, networktype, consensustype) VALUES (1, 'name1', 'd1', 'Mastercard Blockchain', 'Proof of Audit') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.network(id, name, description, networktype, consensustype) VALUES (2, 'name2', 'd2', 'Ethereum', 'Proof of Work') ON CONFLICT DO NOTHING;

SELECT setval('networkdb.networkseq', (SELECT GREATEST((select nextval('networkdb.networkseq')), 3)));

INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (1, 1, md5('hashAddress1'), 'd1', 'nodeName1', 'pubKey1', 'audit') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (2, 1, md5('hashAddress2'), 'd2', 'nodeName2', 'pubKey2', 'audit') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (3, 1, md5('hashAddress3'), 'd3', 'nodeName3', 'pubKey3', 'audit') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (4, 1, md5('hashAddress4'), 'd4', 'nodeName4', 'pubKey4', 'customer') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (5, 1, md5('hashAddress5'), 'd5', 'nodeName5', 'pubKey5', 'customer') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (6, 1, md5('hashAddress6'), 'd6', 'nodeName6', 'pubKey6', 'customer') ON CONFLICT DO NOTHING;

INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (7, 2, md5('hashAddress7'), 'd7', 'nodeName7', 'pubKey7', 'audit') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (8, 2, md5('hashAddress8'), 'd8', 'nodeName8', 'pubKey8', 'audit') ON CONFLICT DO NOTHING;
INSERT INTO networkdb.node(id, networkid, address, description, name, "publickey", nodetype) VALUES (9, 2, md5('hashAddress9'), 'd9', 'nodeName9', 'pubKey9', 'audit') ON CONFLICT DO NOTHING;

SELECT setval('networkdb.nodeseq', (SELECT GREATEST((select nextval('networkdb.nodeseq')), 10)));
