/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.businessobject;


public class Node {

    private Long id;
    private String name;
    private String description;
    private String address;
    private String publicKey;
    private String nodeType;


    public Node() {
    }

    public Node(Long id, String name, String description, String address, String publicKey, String nodeType) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.publicKey = publicKey;
        this.nodeType = nodeType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }
}
