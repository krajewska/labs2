package com.mastercard.labs.network.web.service;

import com.mastercard.labs.network.web.businessobject.Network;
import com.mastercard.labs.network.web.businessobject.Node;
import com.mastercard.labs.network.web.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class NetworkService {

    @Autowired
    private NetworkRepository networkRepository;

    @Autowired
    private NodeRepository nodeRepository;


    public List<Network> getAllNetworks() {

        List<NetworkEntity> networkList = networkRepository.findAll();

        List<Network> networkDtoList =
                networkList.stream()
                        .map(n -> new Network(n.getId(), n.getName(), n.getDescription(), n.getConsensustype(), n.getNetworktype()))
                        .collect(Collectors.toList());

        networkDtoList.forEach(dto -> {
            dto.setNodes(getNodeList(dto.getId()));
        });
        return networkDtoList;
    }

    public Network getNetworkById(Long networkId) {
        Optional<NetworkEntity> network = networkRepository.findById(networkId);
        if (!network.isPresent()) {
            throw new NetworkNotFoundException("No network found: " + networkId);
        }

        NetworkEntity networkEntity = network.get();
        Network networkDto = new Network(networkEntity.getId(), networkEntity.getName(), networkEntity.getDescription(), networkEntity.getConsensustype(), networkEntity.getNetworktype());

        List<NodeEntity> nodeEntities = nodeRepository.findByNetworkid(networkEntity.getId());
        networkDto.setNodes(mapNodeEntitiesToDto(nodeEntities));
        return networkDto;
    }


    public Long createNetwork(Network networkDto) {
        NetworkEntity networkEntity = new NetworkEntity(null, networkDto.getName(), networkDto.getDescription(), networkDto.getConsensusType(), networkDto.getNetworkType());
        NetworkEntity savedNetwork = networkRepository.save(networkEntity);

        if (!CollectionUtils.isEmpty(networkDto.getNodes())){
            for (Node nodeDto: networkDto.getNodes()) {
                NodeEntity nodeEntity = new NodeEntity(null, savedNetwork.getId(), nodeDto.getAddress(), nodeDto.getDescription(), nodeDto.getName(), nodeDto.getPublicKey(), nodeDto.getNodeType());
                nodeRepository.save(nodeEntity);
                }
        }
        return savedNetwork.getId();
    }


    public void updateNetwork(Network networkDto, Long networkId) {
        Optional<NetworkEntity> networkOptional = networkRepository.findById(networkId);
        if (!networkOptional.isPresent()) {
            throw new NetworkNotFoundException("No network found: " + networkId);
        }

        NetworkEntity networkEntity = networkOptional.get();
        networkEntity.setName(networkDto.getName());
        networkEntity.setDescription(networkDto.getDescription());
        networkEntity.setConsensustype(networkDto.getConsensusType());
        networkEntity.setNetworktype(networkDto.getNetworkType());
        networkRepository.save(networkEntity);
    }


    public void deleteNetwork(Long networkId) {
        nodeRepository.removeByNetworkid(networkId);
        networkRepository.deleteById(networkId);
    }


    public List<Node> getNodeList(Long networkId) {
        List<NodeEntity> nodeEntities = nodeRepository.findByNetworkid(networkId);
        List<Node> nodeDtoList = mapNodeEntitiesToDto(nodeEntities);
        return nodeDtoList;
    }

    public Node getNode(Long nodeId) {
        Optional<NodeEntity> nodeOptional = nodeRepository.findById(nodeId);
        if (!nodeOptional.isPresent()) {
            throw new NetworkNotFoundException("No node found: " + nodeId);
        }
        NodeEntity nodeEntity = nodeOptional.get();
        Node nodeDto = new Node(nodeEntity.getId(), nodeEntity.getName(), nodeEntity.getDescription(), nodeEntity.getAddress(), nodeEntity.getPublickey(), nodeEntity.getNodetype());
        return nodeDto;
    }


    public Long addNode(Node node, Long networkId) {
        NodeEntity nodeEntity = new NodeEntity(null, networkId, node.getAddress(), node.getDescription(), node.getName(), node.getPublicKey(), node.getNodeType());
        NodeEntity savedNode = nodeRepository.save(nodeEntity);
        return savedNode.getId();
    }

    public void updateNode(Node nodeDto, Long nodeId) {
        Optional<NodeEntity> nodeOptional = nodeRepository.findById(nodeId);
        if (!nodeOptional.isPresent()) {
            throw new NetworkNotFoundException("No node found: " + nodeId);
        }
        NodeEntity nodeEntity = nodeOptional.get();
        nodeEntity.setAddress(nodeDto.getAddress());
        nodeEntity.setDescription(nodeDto.getDescription());
        nodeEntity.setName(nodeDto.getName());
        nodeEntity.setPublickey(nodeDto.getPublicKey());
        nodeEntity.setNodetype(nodeDto.getNodeType());
        nodeRepository.save(nodeEntity);
    }

    public void deleteNode(Long nodeId) {
        nodeRepository.deleteById(nodeId);
    }


    private List<Node> mapNodeEntitiesToDto(List<NodeEntity> nodeEntities) {
        if (CollectionUtils.isEmpty(nodeEntities)) {
            return new ArrayList<>();
        }
        List<Node> nodeDtoList = nodeEntities.stream()
                .map(n -> new Node(n.getId(), n.getName(), n.getDescription(), n.getAddress(), n.getPublickey(), n.getNodetype()))
                .collect(Collectors.toList());
        return nodeDtoList;
    }
}
