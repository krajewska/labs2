/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.repository;

public class NetworkNotFoundException extends RuntimeException {

	public NetworkNotFoundException(String exception) {
		super(exception);
	}

}
