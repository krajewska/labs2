/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.repository;


import javax.persistence.*;


@Entity
@Table(name = "NETWORK")
public class NetworkEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "network_generator")
    @SequenceGenerator(name = "network_generator", sequenceName = "networkseq", allocationSize = 1)
    private Long id;
    private String name;
    private String description;
    private String consensustype;
    private String networktype;


    public NetworkEntity() {
    }

    public NetworkEntity(Long id, String name, String description, String consensustype, String networktype) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.consensustype = consensustype;
        this.networktype = networktype;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConsensustype() {
        return consensustype;
    }

    public void setConsensustype(String consensustype) {
        this.consensustype = consensustype;
    }

    public String getNetworktype() {
        return networktype;
    }

    public void setNetworktype(String networktype) {
        this.networktype = networktype;
    }
}
