/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.businessobject;


import java.util.ArrayList;
import java.util.Collection;


public class Network {

    private Long id;
    private String name;
    private String description;
    private String consensusType;
    private String networkType;
    private Collection<Node> nodes;

    public Network() { }

    public Network(Long id, String name, String description, String consensusType, String networktype) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.consensusType = consensusType;
        this.networkType = networktype;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConsensusType() {
        return consensusType;
    }

    public void setConsensusType(String consensusType) {
        this.consensusType = consensusType;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public Collection<Node> getNodes() {
        return nodes;
    }

    public void setNodes(Collection<Node> nodes) {
        this.nodes = nodes;
    }

}
