/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.repository;


import javax.persistence.*;


@Entity
@Table(name = "node")
public class NodeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "node_generator")
    @SequenceGenerator(name = "node_generator", sequenceName = "nodeseq", allocationSize = 1)
    private Long id;
    private Long networkid;
    private String address;
    private String description;
    private String name;
    private String publickey;
    private String nodetype;


    public NodeEntity() {
    }

    public NodeEntity(Long id, Long networkid, String address, String description, String name, String publickey, String nodetype) {
        this.id = id;
        this.networkid = networkid;
        this.address = address;
        this.description = description;
        this.name = name;
        this.publickey = publickey;
        this.nodetype = nodetype;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNetworkid() {
        return networkid;
    }

    public void setNetworkid(Long networkid) {
        this.networkid = networkid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public String getNodetype() {
        return nodetype;
    }

    public void setNodetype(String nodetype) {
        this.nodetype = nodetype;
    }
}
