/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.controller;

import java.net.URI;
import java.util.List;

import com.mastercard.labs.network.web.businessobject.Network;
import com.mastercard.labs.network.web.service.NetworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mastercard.labs.network.web.repository.NetworkRepository;
import com.mastercard.labs.network.web.repository.NodeRepository;

@RestController
@Api(value = "NetworkNode Management System", description = "NetworkNode Management System")
public class NetworkController {

    @Autowired
    private NetworkRepository networkRepository;

    @Autowired
    private NodeRepository nodeRepository;

    @Autowired
    private NetworkService networkService;

    @GetMapping("/network")
    @ApiOperation(value = "View a list of NetworkNodes", response = List.class)
    public ResponseEntity<List<Network>> retrieveAllNetworks() {
        try {

            List<Network> networkDtoList = networkService.getAllNetworks();
            return ResponseEntity.ok(networkDtoList);

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/network/{networkId}")
    @ApiOperation(value = "View Network by Id", response = Network.class)
    public ResponseEntity<Network> retrieveNetwork(@PathVariable Long networkId) {
        try {

            Network networkDto = networkService.getNetworkById(networkId);
            return ResponseEntity.ok(networkDto);

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping("/network")
    @ApiOperation(value = "Add Network")
    public ResponseEntity<Object> createNetwork(@RequestBody Network network) {
        Long networkId = networkService.createNetwork(network);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{networkId}")
                .buildAndExpand(networkId).toUri();
        return ResponseEntity.created(location).build();
    }


    @PutMapping("/network/{networkId}")
    @ApiOperation(value = "Modify Network")
    public ResponseEntity<Object> updateNetwork(@RequestBody Network networkDto, @PathVariable long networkId) {
        try {

            networkService.updateNetwork(networkDto, networkId);
            return ResponseEntity.noContent().build();

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    @DeleteMapping("/network/{networkId}")
    @ApiOperation(value = "Remove Network")
    public ResponseEntity<Object> deleteNetwork(@PathVariable long networkId) {
        networkService.deleteNetwork(networkId);
        return ResponseEntity.noContent().build();
    }


}
