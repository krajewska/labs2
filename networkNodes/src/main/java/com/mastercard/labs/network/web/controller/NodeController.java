/*
 * Copyright (c) 2018 Mastercard. All rights reserved.
 */

package com.mastercard.labs.network.web.controller;

import com.mastercard.labs.network.web.businessobject.Node;
import com.mastercard.labs.network.web.repository.NetworkRepository;
import com.mastercard.labs.network.web.repository.NodeRepository;
import com.mastercard.labs.network.web.service.NetworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@Api(value = "NetworkNode Management System", description = "NetworkNode - Node Management System")
public class NodeController {

    @Autowired
    private NetworkRepository networkRepository;

    @Autowired
    private NodeRepository nodeRepository;

    @Autowired
    private NetworkService networkService;


    @GetMapping("/network/{networkId}/nodes")
    @ApiOperation(value = "View list of Nodes", response = List.class)
    public ResponseEntity<List<Node>> retrieveNetworkNodes(@PathVariable Long networkId) {
        try {

            List<Node> nodeDtoList = networkService.getNodeList(networkId);
            return ResponseEntity.ok(nodeDtoList);

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping("/network/{networkId}/nodes")
    @ApiOperation(value = "Add Node")
    public ResponseEntity<Object> addNode(@RequestBody Node node, @PathVariable long networkId) {
        Long nodeId = networkService.addNode(node, networkId);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(nodeId)
                .toUri();
        return ResponseEntity.created(location).build();
    }


    @GetMapping("/network/{networkId}/nodes/{nodeId}")
    @ApiOperation(value = "View Node")
    public ResponseEntity<Node> getNode(@PathVariable long networkId, @PathVariable long nodeId) {
        try {

            Node nodeDto = networkService.getNode(nodeId);
            return ResponseEntity.ok(nodeDto);

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/network/{networkId}/nodes/{nodeId}")
    @ApiOperation(value = "Modify Node")
    public ResponseEntity<Object> updateNode(@RequestBody Node node, @PathVariable long nodeId) {
        try {

            networkService.updateNode(node, nodeId);
            return ResponseEntity.noContent().build();

        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    @DeleteMapping("/network/{networkId}/nodes/{nodeId}")
    @ApiOperation(value = "Delete Node")
    public ResponseEntity<Object> deleteNode(@PathVariable long nodeId) {
        networkService.deleteNode(nodeId);
        return ResponseEntity.noContent().build();
    }

}
